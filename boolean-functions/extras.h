#pragma once

// Counts all right zeroes in UNSIGNED INT
int count_right_zeroes(unsigned int );

// Converts byte to c-string
const char * uint_to_binary(unsigned int );

// Reverses c-string
void reverse(char [] );

// byte weight 
unsigned int count_bits(unsigned int n);

// amount of combinations
unsigned long long
choose(unsigned long long n, unsigned long long k);