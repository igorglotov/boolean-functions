#pragma once
#include <fstream>

using namespace std;

typedef unsigned int BASE;

enum V_TYPE { ZERO, FULL, RAND };

const unsigned int BASE_SIZE_BITS = sizeof(BASE) * 8;
const unsigned int BASE_SIZE = sizeof(BASE);

class BoolF {
public:
	BoolF();
	BoolF(BoolF & );
	BoolF(const char * );
	BoolF(V_TYPE , unsigned int );
    ~BoolF();

private:
	void getMemory(unsigned int );
	void getString(char * , int );

	BASE * value;
	int size_bases;
	int args;
	unsigned int size_bits; // always a power of 2: 1<<args

public:

	// operators
	friend ostream& operator << (ostream & , BoolF & );

	bool operator == (BoolF &) const;
	bool operator != (BoolF &) const;
	BoolF operator = (BoolF &);

	// functions
	int get_bit(int num) const;
	void set_bit(unsigned int num);
	int getSizeBits() { return size_bits; };
	int weight();
	int num_args();
    void invert();
    
	string anf_string();
	int * green(int & length);
    int64_t * autocorrelation(int & length);
	unsigned int find_correlation_immune_deg();
    unsigned int find_propagation_cr_deg(); 
    unsigned int find_SAC_deg();
    
	unsigned int find_nonlinearity();
	unsigned int find_flawed_nonlinearity();
    BoolF best_affine_approx();
    unsigned int hamming_distance(BoolF & func);

	BoolF mobius();
	unsigned int degree();

};
