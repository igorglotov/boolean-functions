//
//  main.cpp
//  boolean-functions
//
//  Created by Igor Glotov on 4/22/12.
//  Copyright (c) 2012 igor.n.glotov@gmail.com. All rights reserved.


#include "stdafx.h"


int main(int argc, char * argv[]) {
	srand((unsigned int) time(NULL));
    
    /*
	BoolF BF("10000000000000000000000000000001011111111111111111111111111111101000000000000000000000000000000101111111111111111111111111111110");
    int len;
    int * values = BF.green(len);
    
    cout << "WA for " << BF << " is: ";
    
    for (int j=0; j<len; j++) {
        cout << values[j] << " ";
    }*/
    
    //BoolF affine("11111110");
    //BoolF && approx = affine.best_affine_approx();
    //cout << "int=" << approx << endl;
    //cout << "nonlinearity: " << affine.find_nonlinearity() << endl;

    //cout << affine << endl;
    //affine.invert();
    //cout << affine << endl;
    //BoolF func("1011011000010111");

    
    //BoolF func("0001000100011110");
    //BoolF func("0001011101111110");

    
    
    for (int i=10; i<31; i++) {
        BoolF func(RAND, i);
        clock_t clk = clock();

        func.find_SAC_deg();
        printf("%d - done in %lu sec\n", i, (clock() - clk)/CLOCKS_PER_SEC);

    }
    
	/*
    for (int i=6;i<9; i++) {
        int len;
        BoolF && BF = BoolF(RAND, i);
        int * values = BF.green(len);
        
        cout << "WA for " << BF << " is: ";
        
        for (int j=0; j<len; j++) {
            cout << values[j] << " ";
        }
        
        cout << endl;
    }*/
	/*
     for (int i=15; i<20; i++) {
     cout << "i=" << i << endl;
     BoolF BF(RAND, i);
     std::cout << "power for " << i << " is " << BF.degree() << std::endl;
     }*/
    
	/*
	BoolF BF("0001000100011110");
	int len;
	int64_t * green_ptr = BF.autocorrelation(len);
	
    
	cout << "Autocorrelation: ";
	for (int i=0; i<len; i++) {
		cout << green_ptr[i];
	}
    
	cout << endl << "Immune deg: " << BF.find_correlation_immune_deg() << endl;
	cout << "Nonlinearity: " << BF.find_nonlinearity() << endl;
    cout << "Flawed Nonlinearity: " << BF.find_flawed_nonlinearity() << endl;
	*/
    
    /*
    for (int i=10; i<30; i++) {
         printf("iteration %d ... ", i);
         BoolF && BF = BoolF(V_TYPE::RAND, i);
         clock_t clk = clock();
         int val = BF.find_flawed_nonlinearity();
         printf(" - done in %lu sec, value is %d\n", (clock() - clk)/CLOCKS_PER_SEC, val);
     }*/
    
    
	/*
     BoolF BF = BoolF(RAND, 20);
     clock_t c0 = clock();
     
     BoolF Mob = BF.mobius();
     
     c0 = clock() - c0;
     unsigned int time = c0 / CLOCKS_PER_SEC;
     */
	//std::cout << "Mobius for " << BF << " is " << Mob << std::endl;
	//printf("Total time: %u sec.\n", time);

    return 0;
}