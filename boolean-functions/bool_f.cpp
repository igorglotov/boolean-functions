#include "stdafx.h"

#define STRBUFFERLENGTH 256
#define FORMATTED_OUTPUT

using namespace std;

void BoolF::getMemory(unsigned int num_bases) {
	try {
		value = new BASE[num_bases];
	} catch (exception & e) {
		cout << "Exception occured: " << e.what() << " for " << num_bases << " bases " << endl;
		throw e;
	}
}

void BoolF::getString(char * array, int bufflen) {
	for (int i = 0; i < size_bases; i++) {
		cout << uint_to_binary(value[i]) << " ";
	}
}

ostream& operator<<(ostream &out, BoolF &bn) {
	for (int i = 0; i < bn.size_bases; i++) {
		out << uint_to_binary(bn.value[i]);
#ifdef FORMATTED_OUTPUT
		if (i != bn.size_bases - 1)
			out << " ";
#endif
	}

	return out;
}

//NOTE: Strings are reversed in C
BoolF::BoolF(const char * str) {
	unsigned int len = strlen(str);
	//do not allow size which is not power of 2
	assert( len!=0 && (len&(len-1)) == 0);

	this->args = count_right_zeroes(len);

	//scan string & check symbols
	for (unsigned int i = 0; i < len; i++) {
		assert( str[i]-'0' == 0 || str[i]-'0' == 1);
	}

	//allocate & fill
	int bases_requested = len / BASE_SIZE_BITS
			+ ((len % BASE_SIZE_BITS) ? 1 : 0);
	getMemory(bases_requested);
	size_bases = bases_requested;
	size_bits = len;

	//zero space
	for (int i = 0; i < size_bases; i++)
		value[i] = 0;

	//in case of little function. STL ROCKS.	
	if (size_bases == 1) {
		string bitstr(str);
		value[0] =
				(BASE) bitset<numeric_limits<unsigned long>::digits>(bitstr).to_ulong();
		return;
	}

	//write text bits
	for (unsigned int i = 0; i < size_bits; i++)
		value[i / BASE_SIZE_BITS] |= BASE(
				(str[i] - '0') << (BASE_SIZE_BITS - i % BASE_SIZE_BITS - 1)); // reverse bit direction
}
/*
 * @desc create new function
 * @param bitsize - size of function in bits
 */

BoolF::~BoolF() {
    if (value) {
        delete [] value;
    }
}

//NOTE: size is for power of 2
BoolF::BoolF(V_TYPE type, unsigned int args) {

	this->args = args;
	int bases_requested = (1<<args) / BASE_SIZE_BITS
			+ (((1<<args) % BASE_SIZE_BITS) ? 1 : 0);

	getMemory(bases_requested);
	size_bases = bases_requested;
	size_bits = (1<<args);

	// initial fill
	for (int i = 0; i < size_bases; i++)
		value[i] = 0;

	switch (type) {
	case ZERO:
		for (int i = 0; i < size_bases; i++)
			value[i] = 0;

	break;

	case FULL:
		if (size_bases == 1) {
			value[0] = (std::numeric_limits<BASE>::max()
					>> (BASE_SIZE_BITS - size_bits));
			return;
		}
		for (int i = 0; i < size_bases; i++)
			value[i] = std::numeric_limits<BASE>::max();

	break;

	case RAND:
	default:
		if (size_bases == 1) {
			value[0] = (rand() % numeric_limits<BASE>::max()
					>> (BASE_SIZE_BITS - size_bits));
			return;
		}

		for (int i = 0; i < size_bases; i++)
			value[i] = rand()+rand();
	break;
	}

}

BoolF::BoolF(BoolF & arg) {
	if (this == &arg) {
		puts("SELF ASSIGMENT");
		return;
	}

	this->size_bases = arg.size_bases;
	this->args = arg.args;
	this->size_bits = arg.size_bits;
	this->getMemory(size_bases);

	assert( this->value != NULL);

	for (int i = 0; i < size_bases; i++) {
		this->value[i] = arg.value[i];
	}
}

BoolF BoolF::operator =(BoolF & arg) {
	assert(this != &arg);

	this->size_bases = arg.size_bases;
	this->size_bits = arg.size_bits;

	// memory reallocation
	if (this->value != NULL)
		delete[] this->value;

	this->getMemory(size_bases);

	assert( this->value != NULL);

	for (int i = 0; i < size_bases; i++) {
		this->value[i] = arg.value[i];
	}

	return *this;
}

bool BoolF::operator ==(BoolF & arg) const {
	if (this->size_bits != arg.size_bits)
		return false;

	for (int i = 0; i < size_bases; i++) {
		if (this->value[i] != arg.value[i])
			return false;
	}

	assert(this->size_bases == arg.size_bases);

	return true;
}
bool BoolF::operator !=(BoolF & arg) const {
	if (!(*this == arg))
		return true;
	return false;
}

BoolF::BoolF() {
	this->value = NULL;
	this->size_bases = 0;
	this->size_bits = 0;
}

/*
 * @brief Get function vector weight
 * @return weight of function
 * @note algorith from "Hacker's Delight" by Henry S. Warren, Jr.
 * log(32)
 */
int BoolF::weight() {
	int v_weight=0, i=0, j, s, x;

	while (i < size_bases) {
		s=0;
		int gr = min(size_bases, i+31);
		j = i;

		while (j<gr) {
			x = value[j];
			x = x - ( (x>>1)&0x55555555 );
			x = ( x&0x33333333 ) + ( (x>>2)&0x33333333 );
			x = ( x+(x>>4) ) & 0x0f0f0f0f;
			s = s+x;
			j++;
		}

		s = ( s&0x00ff00ff ) + ( (s>>8)&0x00ff00ff );
		s = ( s&0xffff ) + ( s>>16 );

		v_weight+=s;
		i=i+31;
	}

	return v_weight;
}

int BoolF::num_args() {
	return this->args;
}

int BoolF::get_bit(int num) const {
	unsigned int byte = this->value[num / BASE_SIZE_BITS];
	unsigned int bitmask = 1 << (num % BASE_SIZE_BITS);
    return (byte & bitmask) ? 1 : 0;
}

void BoolF::set_bit(unsigned int num) {
    assert(num < size_bits);
	this->value[num / BASE_SIZE_BITS] |= (1 << num);
}

BoolF BoolF::mobius() {
	assert(this->value != nullptr);

	BoolF Func(ZERO, this->num_args());
	
	unsigned int bit = 0;
	bit ^= this->get_bit(this->size_bits-1);
	if (bit == 1)
			Func.set_bit(this->size_bits-1);

	for (unsigned int i=1; i<this->size_bits; i++) {
		int a=i, x=i;
		bit = this->get_bit(this->size_bits-i-1);
		
		while(x!=0) {
			x = (x-1)&a;
			bit ^= this->get_bit(this->size_bits-x-1);
		}

		if (bit == 1)
			Func.set_bit(this->size_bits-i-1);
	}
	
	return Func;
}

string BoolF::anf_string() {
	const BoolF& Mob = this->mobius();
	string buffer;

	if (get_bit(this->size_bits-1) == 1) {
		buffer+='1';
	}

	int M = 24; // letters to use

	for (unsigned int i = 1; i <= Mob.size_bits-1; i++) {
		if (Mob.get_bit(Mob.size_bits-i-1) == 1) {
			buffer += '+';
			for (int j = Mob.size_bits, k = M; j > 0; j /= 2, k--) {
				if ((i & j) > 0) {
					buffer += (char)(64 - k + M);
				}
			}
		}
	}

	return buffer;
}

unsigned int BoolF::degree() {
	const BoolF & Mob = this->mobius();

	assert(Mob.size_bits == this->size_bits);

	unsigned int max_power = 0, power;

	for (unsigned int i=0; i<Mob.size_bits; i++) {
		if (Mob.get_bit(i)) {
			power = count_bits(Mob.size_bits-i-1);
			max_power = (power > max_power) ? power : max_power;
		}
	}

	return max_power;
}

/*
 * @brief Green scheme to find Walshe-Adamar's coefficients vector
 * @return arr - pointer to array of integer
 *		   length - array length
 */

int * BoolF::green(int & length) {
	int * arr_t, length_t=this->getSizeBits();

#if !defined _WIN64
	try {
		arr_t = new int[length_t];
	} catch (exception & e) {
		cout << "Caught " << e.what() << " allocating " << length_t << " size int array " << endl;
		throw e;
	}
#else 
		arr_t = (int *) VirtualAlloc(NULL, length_t*sizeof(int), MEM_COMMIT, PAGE_READWRITE);
		if (arr_t == NULL) {
			DWORD code = GetLastError();
			printf("Error allocating memory: %d", code);
		}
#endif
	// find characteristic vector
	// NOTE: DUMMY
	for (int i=0; i<length_t; i++) {
		if (this->get_bit(length_t-1-i))
			arr_t[i]=-1;
		else 
			arr_t[i]=1;
	}

	int * arr_c;
#if !defined _WIN64
	try {
		arr_c = new int[length_t];
	} catch (exception & e) {
		cout << "Caught " << e.what() << " allocating " << length_t << " size int array " << endl;
		throw e;
	}
#else 
		arr_c = (int *) VirtualAlloc(NULL, length_t*sizeof(int), MEM_COMMIT, PAGE_READWRITE);
#endif

	
	for (int i=0; i<length_t; i++) {
		arr_c[i] = arr_t[i];
	}
	

	for (int i=0; i<this->args; i++) {
		int step = 1<<i;
		int dstep = 2*step;
		
		/*
		//memcpy();
		for (int i=0; i<length_t; i++) {
			arr_t[i] = arr_c[i];
		}*/
		std::swap(arr_t, arr_c);

		for (int k=0; k<(length_t-step); k+=dstep) {

			for (int l=0; l<step; l++) {
				arr_c[k+l] = arr_t[k+l+step]+arr_t[k+l];
			}

			for (int m=step; m<dstep; m++) {
				arr_c[k+m] = arr_t[m-step+k]-arr_t[k+m];
			}
		}
	}


#if !defined _WIN64
	delete [] arr_t;
	//delete [] arr_c;
#else 
	//if (VirtualFree(arr_c, NULL, MEM_RELEASE) == 0)
	//	printf("Problem deallocating %d*4 bytes", length_t);
	if (VirtualFree(arr_t, NULL, MEM_RELEASE) == 0)
		printf("Problem deallocating %d*4 bytes", length_t);
#endif

	length = length_t;
	return arr_c;
}

unsigned int BoolF::find_correlation_immune_deg() {
	int green_len;
	int * green_arr = this->green(green_len);

	assert(green_len == this->size_bits);

	unsigned int deg = 0, m = this->num_args();

	for (int i=1; i<m; i++) {
		bool check_failed = false;
		unsigned int a = ( (1<<i)-1 )<<( this->num_args()-i );

		if (green_arr[a] != 0)
			continue;

		// build all transpositions: Zakrevsky alg.

		while ( a!=((1<<i)-1) ) {
			unsigned int b = (a+1)&a;
			unsigned int c = count_bits((b-1)^a)-2;
			a = ( ( ( ( (a+1)^a )<<1 )+1 )<<c )^b;
			if (green_arr[a] != 0) {
				check_failed = true;
				break;
			}
		}

		if (!check_failed)
			deg = i;

	}

	return deg;
}

unsigned int BoolF::find_nonlinearity() {
	int green_len;
	int * green_arr = this->green(green_len);

	assert(green_len == this->size_bits);

	long max_element = std::labs(green_arr[0]);
	for (unsigned int i=1; i<this->size_bits; i++)
		max_element = std::max(std::labs(green_arr[i]), max_element);
	
	return ( (1<<(this->num_args()-1))-(max_element>>1) ) ;
}

int64_t * BoolF::autocorrelation(int & length) {
    int green_len;
	int * green_arr = (int *) this->green(green_len);
    length = green_len;
	int64_t * green_arr_c, * green_arr_t;
    
#if !defined _WIN64
	try {
		green_arr_c = new int64_t[green_len];
        green_arr_t = new int64_t[green_len]; 
	} catch (exception & e) {
		cout << "Caught " << e.what() << " allocating " << green_len << " size int array " << endl;
		throw e;
	}
#else 
	green_arr_c = (INT64 *) VirtualAlloc(NULL, green_len*sizeof(INT64), MEM_COMMIT, PAGE_READWRITE);
    green_arr_t = (INT64 *) VirtualAlloc(NULL, green_len*sizeof(INT64), MEM_COMMIT, PAGE_READWRITE);
#endif
    for (int i=0; i<green_len; i++) {
        green_arr_c[i] = (int64_t) green_arr[i];
    }
    
    for (int i=0; i<green_len; i++) {
        green_arr_t[i] = (green_arr_c[i]*green_arr_c[i]) >> (this->args);
    }
    
    for (int i=0; i<green_len; i++)
        green_arr_c[i] = green_arr_t[i];
    
    delete [] green_arr;
    
    for (int i=0; i<this->args; i++) {
		int step = 1<<i;
		int dstep = 2*step;
        
        std::swap(green_arr_t, green_arr_c);
        
		for (int k=0; k<(green_len-step); k+=dstep) {
            
			for (int l=0; l<step; l++) {
				green_arr_c[k+l] = green_arr_t[k+l+step]+green_arr_t[k+l];
			}
            
			for (int m=step; m<dstep; m++) {
				green_arr_c[k+m] = green_arr_t[m-step+k]-green_arr_t[k+m];
			}
		}
	}
    
	std::swap(green_arr_t, green_arr_c);
    
#if !defined _WIN64
	delete [] green_arr_c;
	//delete [] arr_c;
#else 
	//if (VirtualFree(arr_c, NULL, MEM_RELEASE) == 0)
	//	printf("Problem deallocating %d*4 bytes", length_t);
	if (VirtualFree(green_arr_c, NULL, MEM_RELEASE) == 0)
		printf("Problem deallocating %d*4 bytes", green_len);
#endif
    
    return green_arr_t;
     
}

unsigned int BoolF::find_flawed_nonlinearity() {
	int autocorrelation_len;
    int64_t * autocorrelation_arr = this->autocorrelation(autocorrelation_len);

	int64_t max_element = (int64_t) std::abs(autocorrelation_arr[1]);
	for (unsigned int i=2; i<this->size_bits; i++)
		max_element = std::max(std::llabs(autocorrelation_arr[i]), max_element);

#if !defined _WIN64
	delete [] autocorrelation_arr;
#else 
	if (VirtualFree(autocorrelation_arr, NULL, MEM_RELEASE) == 0)
		printf("Problem deallocating %d*4 bytes", green_len);
#endif
	return (1<<(this->args-2))-(max_element>>2);
}

void BoolF::invert() {
    for (int i=0; i<size_bases; i++) {
        value[i] = ~value[i];
    }
    
    // sooooo dirty :]
    if (size_bits < 32) {
        value[0] = value[0] & (0xffffffff >> 32-size_bits);
    }
}

unsigned int BoolF::hamming_distance(BoolF &func) {
    unsigned int dist = 0, val;
    
    for (unsigned int i=0; i<this->size_bases; i++) {
        val = this->value[i] ^ func.value[i];
        // count the number of set bits
        dist += count_bits(val);
    }
    return dist;
}

BoolF BoolF::best_affine_approx() {
    int green_len;
    int * green_arr = this->green(green_len);
    
    int max_element = std::abs(green_arr[0]);
    unsigned int max_index=0;
	for (unsigned int i=1; i<this->size_bits; i++) {
        int new_max = std::max(std::abs(green_arr[i]), max_element);
		if (max_element < new_max) {
            max_element = new_max;
            max_index = i;
        }
    }
    
    bool should_invert = (green_arr[max_index] < 0) ? true : false;
    
    delete [] green_arr;
    
    BoolF affine(ZERO, this->args);
    
    // now we got max element and it's index
    for (unsigned int i=0; i<this->size_bits; i++) {
        // xor argument bits
        unsigned int filtered_i = (i&max_index);
        filtered_i = filtered_i^(filtered_i>>1);
        filtered_i = filtered_i^(filtered_i>>2);
        filtered_i = filtered_i^(filtered_i>>4);
        filtered_i = filtered_i^(filtered_i>>8);
        filtered_i = filtered_i^(filtered_i>>16);
        
        // if xor = 1
        if (filtered_i & 0x1)
            affine.set_bit(this->size_bits-i-1);
        
    }
    
    if (should_invert)
        affine.invert();
    
    //assert( this->hamming_distance(affine) == this->find_nonlinearity() );
    
    return affine;
}

unsigned int BoolF::find_propagation_cr_deg() {
    int autoc_len;
	int64_t * autoc_arr = this->autocorrelation(autoc_len);
    
	assert(autoc_len == this->size_bits);
    
	unsigned int deg = 0, m = this->num_args();
    
	for (int i=1; i<=m; i++) {
		bool check_failed = false;
		unsigned int a = ( (1<<i)-1 )<<( this->num_args()-i );
        
		if (autoc_arr[a] != 0)
			return deg;
        
		// build all transpositions: Zakrevsky alg.
        
		while ( a!=((1<<i)-1) ) {
			unsigned int b = (a+1)&a;
			unsigned int c = count_bits((b-1)^a)-2;
			a = ( ( ( ( (a+1)^a )<<1 )+1 )<<c )^b;
			if (autoc_arr[a] != 0) {
				check_failed = true;
				break;
			}
		}
        
        // last step 
        
        if (autoc_arr[a] != 0)
            return deg;
        
        
		if (!check_failed)
			deg = i;
        else 
            break;
        
	}
    
	return deg;
}

unsigned int BoolF::find_SAC_deg() {
    
    int autoc_len;
	int64_t * autoc_arr = this->autocorrelation(autoc_len);
    int source_func_len = this->getSizeBits();
    
    unsigned int num_args = this->num_args();
    
    // FIXME: default SAC skipped
    
    for (int i=1; i<autoc_len; i<<=1) {
        if (autoc_arr[i] != 0)
            return 0;
    }

    for (int i=1; i<num_args; i++) { // ???
        unsigned int sub_func_len = this->args - i;
        
        unsigned int fixed_by_sac_args = ( (1<<i)-1 )<<( num_args-i );
        
        const unsigned int new_args = (~ fixed_by_sac_args) & (UINT_MAX >> (sizeof(unsigned int)*8-num_args));
        
        // Zakrevksy alg. start: until new_args!=((1<<i)-1)
        
        // combinations number
        unsigned long long num_comb = choose(num_args, sub_func_len);
        while (num_comb--) {
            
            unsigned int x = fixed_by_sac_args;

            // all bit combinations start (fixations)
            unsigned int comb_count = 1 << i;
            while (comb_count--) {
                unsigned int next_bit = 0;
                BoolF sub_func(ZERO, sub_func_len);
                
            
                for (int j=0; j<source_func_len; j++) { 
                    
                    // new autocorr
                    if ( (x&j) == x && (j&fixed_by_sac_args) == x ) {
                        if (this->get_bit(source_func_len-j-1)) {
                            sub_func.set_bit(next_bit++);
                        } else {
                            next_bit++;
                        }
                    }
                }
                
                int sub_func_autoc_len;
                int64_t * sub_func_autoc_arr = sub_func.autocorrelation(sub_func_autoc_len);
                
                for (int l=1; l<sub_func_autoc_len; l<<=1) {
                    if (sub_func_autoc_arr[l] != 0) {
                        return i-1;
                    }
                }
            
                x= (x-1) & fixed_by_sac_args;
                delete [] sub_func_autoc_arr;
            
            } ;; 
                    
            // zakrevsky recalc transpose:
            unsigned int b = (fixed_by_sac_args+1)&fixed_by_sac_args;
			unsigned int c = count_bits((b-1)^fixed_by_sac_args)-2;
			fixed_by_sac_args = ( ( ( ( (fixed_by_sac_args+1)^fixed_by_sac_args )<<1 )+1 )<<c )^b;
        
        }
                
    }
    
    return 0;
}