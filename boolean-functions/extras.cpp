#include "stdafx.h"

// Counts all right zeroes in UNSIGNED INT
int count_right_zeroes(unsigned int num) {
	int n=0;

	if ((num<<16) == 0) {
		n+=16;
		num>>=16;
	}

	if ((num<<24) == 0) {
		n+=8;
		num>>=8;
	}

	if ((num<<28) == 0) {
		n+=4;
		num>>=4;
	}

	if ((num<<30) == 0) {
		n+=2;
		num>>=2;
	}

	return ( (num&1) == 1 ) ? n : n+1;
}

// Reverses c-string
void reverse(char p[]) {
	size_t len=strlen(p);
	char t;
	for(size_t i=(--len), j=0; i>len/2; i--, j++)
	{
		// exchange elements
		t=p[i];
		p[i]=p[j];
		p[j]=t;
	}
}
// Converts byte to c-string
const char * uint_to_binary(unsigned int x) {
	int uint_size = sizeof(unsigned int)*8;
    static char b[33];
    unsigned int z = x;
	for (int i=0; i<uint_size; i++, z>>=1)
		b[i] = (z&1)? '1' : '0';
	b[32] = 0;
	reverse(b);
    return b;
}

// 4-byte weight: W. Kernighan and Dennis M. Ritchie

unsigned int count_bits(unsigned int n) {     
  unsigned int c; // c accumulates the total bits set in v
  for (c = 0; n; c++) 
    n &= n - 1; // clear the least significant bit set
  return c;
}

// amount of combinations
unsigned long long choose(unsigned long long n, unsigned long long k) {
    if (k > n) {
        return 0;
    }
    unsigned long long r = 1;
    for (unsigned long long d = 1; d <= k; ++d) {
        r *= n--;
        r /= d;
    }
    return r;
}